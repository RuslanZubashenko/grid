import { TemplateRef } from "@angular/core";

export class Column {
    constructor(public template:TemplateRef<any>, public field: string, public title?: string) {}
}
