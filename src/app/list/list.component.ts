import { Component, OnInit, ContentChildren, QueryList } from '@angular/core';
import { Column } from './column';
import { ColumnDirective } from './column.directive';

import { ListDataService } from '../list-data-service'

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  @ContentChildren(ColumnDirective) appColumns : QueryList<ColumnDirective>;
  
  columns: Column[];
  items: any[];

  page: number = 0;
  pageSize: number = 20;
  totalCount: number;

  constructor(private dataService: ListDataService) { }

  ngOnInit() {
    this.loadPage();    
  }

  ngAfterContentInit(){
    this.columns = this.appColumns.map(x => new Column(x.template, x.field, x.title));
  }

  loadPage(){
    this.dataService.getItems().subscribe(x => this.items = x.splice(0, this.pageSize));//Remove slice. Add filtering, sorting and page.
  }
}
