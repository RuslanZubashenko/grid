import { Directive, Input, TemplateRef } from '@angular/core';

@Directive({
  selector: '[appColumn]'
})
export class ColumnDirective {
  @Input() field: string;
  @Input() title: string;

  constructor(public template: TemplateRef<any>) { }
}
