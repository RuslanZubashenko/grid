import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule }    from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './in-memory-data.service';

import { AppComponent } from './app.component';
import { ListComponent } from './list/list.component';
import { ListTableComponent } from './list/list-table/list-table.component';
import { ListPagerComponent } from './list/list-pager/list-pager.component';
import { ColumnDirective } from './list/column.directive';

@NgModule({
  declarations: [
    AppComponent,
    ListComponent,
    ListTableComponent,
    ListPagerComponent,
    ColumnDirective
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, { dataEncapsulation: false }
    )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
