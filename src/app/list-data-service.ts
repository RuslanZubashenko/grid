import { HttpClient, HttpHeaders } from "@angular/common/http";
import { User } from "./user";

import { Observable } from "rxjs";
import { Injectable } from "@angular/core";

@Injectable({
    providedIn: 'root'
})
export class ListDataService {
    constructor(private http: HttpClient) { }

    private usersUrl = 'api/users';//This will be configurable.

    httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };//Not in use yet

    getItems() : Observable<User[]>{
        return this.http.get<User[]>(this.usersUrl);
    }
}