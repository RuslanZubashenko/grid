export class User{
    id: number;
    username: string;
    email: string;
    approved: boolean;
    firstName: string;
    lastName: string;
}