import { InMemoryDbService } from 'angular-in-memory-web-api';

import { User } from './user'

export class InMemoryDataService implements InMemoryDbService{
    createDb() {
        const users: User[] = [
        ];

        for(var i = 0; i < 100; i++)
            users.push({ id: i, username: `user${i}`, email: `user${i}@something.com`, approved: i % 2 == 0, firstName: `First${i}`, lastName: `Last${i}` });

        return {users};
    }
}